
#include <iostream>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <math.h>
#include <vector>
#include <stdlib.h>
#include <random>
#include <string>

using namespace std;
const double Pi = 3.141592653589793238462643383279502884197169399375105820974944;
const double E = 2.7182818284590452353602874713526624977572470936999595749669676;
const double Gamma = 0.5772156649015328606065120900824024310421593359399235988057;

double calc_A3(const double eta, const double h);
double calc_B3(const double eta);
double calc_Ak(const int k, const double eta3, const double h);
double calc_Bk(const int k, const double eta3);

double round_upwards(double x, int places) {
	double factor = pow(10, places);
	return ceil(x * factor) / factor;
}
double round_downwards(double x, int places) {
	if (x < 0)
	{
		return -round_upwards(-x, places);
	}
	double factor = 1 / pow(10, places);
	return (int)(x / factor) * factor;
}
double round(double value, int digits) {
	if (value == 0.0) return 0.0;
	double factor = pow(10.0, digits - ceil(log10(fabs(value))));
	return round(value * factor) / factor;
}

/* Calculates $A_3$ defined in Lemma 1.3 */
double calc_A3(const double eta, const double h) {
	if (eta <= 0) throw std::invalid_argument("eta must be positive.");
	if (h <= 1) throw std::invalid_argument("h must be greater than 1.");

	double cr_lda0 = pow(1.0 / eta + 32.0 * sqrt(eta) * h / (15 * sqrt(Pi)), -1); // cube root of $\lambda_3$
	double delta3 = sqrt(0.5 + 0.5 * sqrt(1 + 3.0 / 8 * sqrt(Pi) * pow(eta, 1.5)));
	return sqrt(1.0 / (eta * h) + 32.0 / (15 * sqrt(Pi)) * sqrt(eta + cr_lda0) + 1.0 / 3 * (eta + cr_lda0) * cr_lda0) * delta3;
}

/* Calculates $B_3$ defined in Lemma 1.3 */
double calc_B3(const double eta) {

	if (eta <= 0) throw std::invalid_argument("eta must be positive.");

	double delta3 = sqrt(0.5 + 0.5 * sqrt(1 + 3.0 / 8 * sqrt(Pi) * pow(eta, 1.5)));
	return sqrt(32) / (sqrt(3) * pow(Pi * eta, 1.0 / 4)) * delta3;
}

/* Calculates $A_k$ defined in equation (1.4) */
double calc_Ak(const int k, const double eta, const double h) {

	if (k < 3) throw std::invalid_argument("k must be >= 3.");
	if (eta <= 0) throw std::invalid_argument("eta must be positive.");
	if (h <= 1) throw std::invalid_argument("h must be greater than 1.");

	double Ak = calc_A3(eta, h);
	for (int j = 3; j < k; ++j) {
		double J = pow(2, j - 1);
		double delta_j = sqrt(1.0 + 2.0 / pow(2337.0, 1.0 - 2.0 / J) * pow(9.0 * Pi * eta / 1024.0, 1.0 / J));
		Ak = delta_j * (pow(h, -1.0 / J) + pow(2.0, 19.0 / 12.0) * (J - 1) / sqrt((2 * J - 1) * (4 * J - 3)) * sqrt(Ak));
	}
	return Ak;
}

/* Calculates $B_k$ defined in equation (1.5) */
double calc_Bk(const int k, const double eta) {

	if (k < 3) throw std::invalid_argument("k must be >= 3.");
	if (eta <= 0) throw std::invalid_argument("eta must be positive.");

	double Bk = calc_B3(eta);
	for (int j = 3; j < k; ++j) {
		double J = pow(2, j - 1);
		double delta_j = sqrt(1.0 + 2.0 / pow(2337.0, 1 - 2.0 / J) * pow(9 * Pi * eta / 1024, 1.0 / J));
		Bk = delta_j * pow(2.0, 3.0 / 2.0) * (J - 1) / sqrt((2 * J - 3) * (4 * J - 5)) * sqrt(Bk);
	}
	return Bk;
}

/* Calculates $D_3$ appearing in Lemma 3.3 */
double calc_D3(double eta2, double h2, double theta2, double theta3, double logt0, double logt1, bool verbose = false) {

	if (logt1 <= logt0) throw std::invalid_argument("logt0 must less than logt1.");
	if (eta2 <= 0) throw std::invalid_argument("eta2 must be positive.");
	if (h2 <= 1) throw std::invalid_argument("h2 must be greater than 1.");
	if (theta2 <= 0) throw std::invalid_argument("theta2 must be positive.");
	if (theta3 <= 0) throw std::invalid_argument("theta3 must be positive.");

	double h3 = h2 / (1 - h2 / theta3 * exp(-27.0 / 82.0 * logt0));
	if (h3 <= 1) return numeric_limits<double>::infinity(); // Check that h_3 > 1

	double A4 = calc_Ak(4, eta2, pow(h3, 4));
	double R = ceil((115.0 / 1394.0 * logt1 - log(theta3 / theta2)) / log(h2)); // R(t_1)
	double D1 = pow(3.0 / Pi, 1.0 / 14.0) * A4 * pow(h3, 5.0 / 7.0) * (h3 - 1); // $D_1$ appearing in the proof of Lemma 3.3
	
	if (verbose) {
		cout << "Calculation of D3: ------------------------" << endl;
		cout << "eta_2 " << eta2 << endl;
		cout << "h3 " << h3 << endl;
		cout << "A4 " << A4 << endl;
		cout << "R " << R << endl;
		cout << "D1 " << D1 << endl;
		cout << "-------------------------------------------" << endl;
	}

	return D1 * pow(theta2, 3.0 / 14.0) * (1 - pow(h2, -3.0 * R / 14.0)) / (pow(h2, 3.0 / 14.0) - 1);
}

/* Calculates $D_4$ appearing in Lemma 3.3 */
double calc_D4(double eta2, double h2, double theta2, double theta3, double logt0, double logt1, bool verbose = false) {

	if (logt1 <= logt0) throw std::invalid_argument("logt0 must less than logt1.");
	if (eta2 <= 0) throw std::invalid_argument("eta2 must be positive.");
	if (h2 <= 1) throw std::invalid_argument("h2 must be greater than 1.");
	if (theta2 <= 0) throw std::invalid_argument("theta2 must be positive.");
	if (theta3 <= 0) throw std::invalid_argument("theta3 must be positive.");

	double h3 = h2 / (1 - h2 / theta3 * exp(-27.0 / 82.0 * logt0));
	if (h3 <= 1) return numeric_limits<double>::infinity(); // Check that h_3 > 1

	double B4 = calc_Bk(4, eta2);
	double R = ceil((115.0 / 1394.0 * logt1 - log(theta3 / theta2)) / log(h2));
	double D2 = pow(Pi / 3, 1.0 / 14.0) * B4 * pow(h3, 2.0 / 7.0) * pow(h3 - 1, 3.0 / 4.0);

	if (verbose) {
		cout << "Calculation of D3: ------------------------" << endl;
		cout << "B4 " << B4 << endl;
		cout << "R " << R << endl;
		cout << "D2 " << D2 << endl;
		cout << "-------------------------------------------" << endl;
	}

	return D2 * pow(theta2, 15.0 / 28.0) * (1 - pow(h2, -15.0 * R / 28.0)) / (pow(h2, 15.0 / 28.0) - 1);
}

/* Calculates $\lim_{t_1 \to \infty}D_3$. This is used to handle the region $t \in [\exp(875), \infty)$. */
double calc_D3_inf(double eta2, double h2, double theta2, double theta3, double logt0, bool verbose = false) {

	if (eta2 <= 0) throw std::invalid_argument("eta2 must be positive.");
	if (h2 <= 1) throw std::invalid_argument("h2 must be greater than 1.");
	if (theta2 <= 0) throw std::invalid_argument("theta2 must be positive.");
	if (theta3 <= 0) throw std::invalid_argument("theta3 must be positive.");

	double h3 = h2 / (1 - h2 / theta3 * exp(-27.0 / 82.0 * logt0));
	if (h3 <= 1) return numeric_limits<double>::infinity(); // Check that h_3 > 1
	double A4 = calc_Ak(4, eta2, pow(h3, 4));
	return pow(3.0 / Pi, 1.0 / 14.0) * A4 * pow(h3, 5.0 / 7.0) * (h3 - 1) * pow(theta2, 3.0 / 14.0) / (pow(h2, 3.0 / 14.0) - 1);
}

/* Calculates $\lim_{t_1 \to \infty}D_4. This is used to handle the region $t \in [\exp(875), \infty)$. */
double calc_D4_inf(double eta2, double h2, double theta2, double theta3, double logt0) {

	if (eta2 <= 0) throw std::invalid_argument("eta2 must be positive.");
	if (h2 <= 1) throw std::invalid_argument("h2 must be greater than 1.");
	if (theta2 <= 0) throw std::invalid_argument("theta2 must be positive.");
	if (theta3 <= 0) throw std::invalid_argument("theta3 must be positive.");

	double h3 = h2 / (1 - h2 / theta3 * exp(-27.0 / 82.0 * logt0));
	if (h3 <= 1) return numeric_limits<double>::infinity(); // Check that h_3 > 1
	double B4 = calc_Bk(4, eta2);
	return pow(Pi / 3, 1.0 / 14.0) * B4 * pow(h3, 2.0 / 7.0) * pow(h3 - 1, 3.0 / 4.0) * pow(theta2, 15.0 / 28.0) / (pow(h2, 15.0 / 28.0) - 1);
}

/* Calculates $\alpha(h)$ defined in the statement of Lemma 3.4 */
double calc_alpha(double h, double theta1, double theta2, double logt0) {

	if (h <= 1) throw std::invalid_argument("h must be greater than 1.");
	if (theta1 <= 0) throw std::invalid_argument("theta1 must be positive.");
	if (theta2 <= 0) throw std::invalid_argument("theta2 must be positive.");

	return sqrt(h - 1 + theta1 * pow(h, 5.0 / 41.0) / pow(theta2, 5.0 / 41.0) * exp(-222.0 / 697.0 * logt0));
}

/* Calculates $q_0$ defined in the statement Lemma 3.4 */
double calc_q0(double h, double theta1, double theta2, double logt0) {

	if (h <= 1) throw std::invalid_argument("h must be greater than 1.");
	if (theta1 <= 0) throw std::invalid_argument("theta1 must be positive.");
	if (theta2 <= 0) throw std::invalid_argument("theta2 must be positive.");

	double q0 = theta1 * pow(theta2 / h, 36.0 / 41.0) * exp(logt0 * 65.0 / 697.0);
	if (q0 < 2) return numeric_limits<double>::infinity(); // Check that q_0 \geq 2
	return q0;
}

/* Calculates $C_1(h)$ defined in the statement of Lemma 3.4 */
double calc_C1(double h, double eta1, double theta1, double theta2, double logt0) {

	if (h <= 1) throw std::invalid_argument("h must be greater than 1.");
	if (eta1 <= 0) throw std::invalid_argument("eta1 must be positive.");
	if (theta1 <= 0) throw std::invalid_argument("theta1 must be positive.");
	if (theta2 <= 0) throw std::invalid_argument("theta2 must be positive.");

	double q0 = calc_q0(h, theta1, theta2, logt0);
	if (q0 < 2) return numeric_limits<double>::infinity(); // Check that q_0 \geq 2

	double A5 = calc_Ak(5, eta1, 76545.0 * sqrt(2) / 107264.0 * pow(h, 9));
	double alpha = calc_alpha(h, theta1, theta2, logt0);

	return alpha * sqrt((h - 1) / theta1 / (1 - 1 / q0)
		+ 0.475 * pow(theta1, 11.0 / 30.0) * A5 * pow(h, 21.0 / 8.0) * (h - 1));
}

/* Calculates $C_2(h)$ defined in the statement of Lemma 3.4 */
double calc_C2(double h, double eta1, double theta1, double theta2, double logt0) {

	if (h <= 1) throw std::invalid_argument("h must be greater than 1.");
	if (eta1 <= 0) throw std::invalid_argument("eta1 must be positive.");
	if (theta1 <= 0) throw std::invalid_argument("theta1 must be positive.");
	if (theta2 <= 0) throw std::invalid_argument("theta2 must be positive.");

	double B5 = calc_Bk(5, eta1);
	double alpha = calc_alpha(h, theta1, theta2, logt0);
	return alpha * sqrt(0.2531 * pow(theta1, 61.0 / 120.0) * B5 * pow(h, 3.0 / 2.0) * pow(h - 1, 7.0 / 8.0));
}

/* Calculates $E_1(h)$ defined in the statement of Lemma 3.4 */
double calc_E1(double h, double theta1, double theta2, double logt0) {

	if (h <= 1) throw std::invalid_argument("h must be greater than 1.");
	if (theta1 <= 0) throw std::invalid_argument("theta1 must be positive.");
	if (theta2 <= 0) throw std::invalid_argument("theta2 must be positive.");

	double q0 = calc_q0(h, theta1, theta2, logt0);
	if (q0 < 2) return numeric_limits<double>::infinity(); // Check that q_0 \geq 2

	return calc_alpha(h, theta1, theta2, logt0) * sqrt(12.496 * sqrt(Pi)) * pow(h, 3.0 / 4.0)
		* pow(1 / (theta1 * (1 - 1 / q0)), 1.0 / 4.0);
}

/* Calculates $E_2(h)$ defined in the statement of Lemma 3.4 */
double calc_E2(double h, double theta1, double theta2, double logt0) {

	if (h <= 1) throw std::invalid_argument("h must be greater than 1.");
	if (theta1 <= 0) throw std::invalid_argument("theta1 must be positive.");
	if (theta2 <= 0) throw std::invalid_argument("theta2 must be positive.");

	double alpha = calc_alpha(h, theta1, theta2, logt0);
	return alpha * sqrt(9.0 / 14.0 * pow(theta1, 1.0 / 3.0) * 
		(4.465 * pow(h, 1.0 / 3.0) * pow(h - 1, 1.0 / 3.0) / (pow(Pi, 4.0 / 3.0) * pow(theta2, 1.0 / 3.0) * exp(7.0 / 51.0 * logt0))
			+ 6.0 / Pi * pow(h, 3) * (h - 1)));
}

/* Calculates $E_3(h)$ defined in the statement of Lemma 3.4 */
double calc_E3(double h, double theta1, double theta2, double logt0) {

	if (h <= 1) throw std::invalid_argument("h must be greater than 1.");
	if (theta1 <= 0) throw std::invalid_argument("theta1 must be positive.");
	if (theta2 <= 0) throw std::invalid_argument("theta2 must be positive.");

	return calc_alpha(h, theta1, theta2, logt0) * sqrt(6 + 5 * log(2) / Pi);
}

/* Calculates $\mu_1(w, h)$ defined in equation (3.24) */
double calc_mu1(double w, double h, double K) {
	if (h <= 1) throw std::invalid_argument("h must be greater than 1.");
	return pow(2 * Pi, -w / 2) * (1 - pow(h, -w * K)) / (pow(h, w) - 1);
}

/* Calculates $\mu_2(w, h)$ defined in equation (3.24) */ 
double calc_mu2(double w, double h, double K, double theta2, double logt0) {
	if (h <= 1) throw std::invalid_argument("h must be greater than 1.");
	if (theta2 <= 0) throw std::invalid_argument("theta2 must be positive.");

	return calc_mu1(w, h, K) * pow(1 - h / (theta2 * exp(logt0 * 7.0 / 17.0)), -w) * pow(h, w * K);
}

/* Calculates $\mu_3(w, h)$ defined in equation (3.26) */
double calc_mu3(double w, double h) {
	if (h <= 1) throw std::invalid_argument("h must be greater than 1.");
	return pow(2 * Pi, -w / 2) / (pow(h, w) - 1);
}

/* Calculates $\mu_4(w, h)$ defined in equation (3.26) */
double calc_mu4(double w, double h, double theta2, double logt0) {
	if (h <= 1) throw std::invalid_argument("h must be greater than 1.");
	if (theta2 <= 0) throw std::invalid_argument("theta2 must be positive.");

	return pow(1 - h / (theta2 * exp(logt0 * 7.0 / 17.0)), -w) *
		(pow(h / theta2, w) - sqrt(2 * Pi) * exp(-logt0 * 3.0 / 34.0)) / (1 - pow(h, -w));
}

/* Calculates $C_4$ defined in equation (3.23) */
double calc_C4(double h1, double eta1, double theta1, double theta2, double logt0, double logt1, bool verbose = false) {

	if (logt1 <= logt0) throw std::invalid_argument("logt0 must less than logt1.");
	if (logt0 < log(100)) throw std::invalid_argument("logt0 must be >= log(100).");
	if (h1 <= 1) throw std::invalid_argument("h1 must be greater than 1.");
	if (eta1 <= 0) throw std::invalid_argument("eta1 must be positive.");
	if (theta1 <= 0) throw std::invalid_argument("theta1 must be positive.");
	if (theta2 <= 0) throw std::invalid_argument("theta2 must be positive.");

	double h0 = h1 / (1 - theta2 * exp(-7.0 / 17.0 * logt0));
	if (h0 <= 1 || h0 > 2) return numeric_limits<double>::infinity(); // Check that 1 < h_0 <= 2

	double C1 = calc_C1(h0, eta1, theta1, theta2, logt0);
	double C2 = calc_C2(h0, eta1, theta1, theta2, logt0);
	double E1 = calc_E1(h0, theta1, theta2, logt0);
	double E2 = calc_E2(h0, theta1, theta2, logt0);
	double E3 = calc_E3(h0, theta1, theta2, logt0);
	double K = ceil((3.0 / 34.0 * logt1 - log(theta2 * sqrt(2 * Pi))) / log(h1));

	if (verbose) {
		cout << "h0 " << h0 << endl;
		cout << "K(t1, h1) " << K << endl;
		cout << "C1(h0) " << C1 << endl;
		cout << "C2(h0) " << C2 << endl;
		cout << "E1(h0) " << E1 << endl;
		cout << "E2(h0) " << E2 << endl;
		cout << "E3(h0) " << E3 << endl;
		cout << "mu_1(5/82, h1) " << calc_mu1(5.0 / 82.0, h1, K) << endl;
		cout << "mu_2(17/328, h1) " << calc_mu2(17.0 / 328.0, h1, K, theta2, logt0) << endl;
		cout << "mu_1(87/164, h1) " << calc_mu1(87.0 / 164.0, h1, K) << endl;
		cout << "mu_2(5/246, h1) " << calc_mu2(5.0 / 246.0, h1, K, theta2, logt0) << endl;
		cout << "T1 " << C1 * calc_mu1(5.0 / 82.0, h1, K) << endl;
		cout << "T2 " << C2 * calc_mu2(17.0 / 328.0, h1, K, theta2, logt0) * exp(-3.0 / 656.0 * logt0) << endl;
		cout << "T3 " << E1 * calc_mu1(87.0 / 164.0, h1, K) * exp(-27.0 / 328.0 * logt0) << endl;
		cout << "T4 " << E2 * calc_mu2(5.0 / 246.0, h1, K, theta2, logt0) * exp(-13.0 / 246.0 * logt0) << endl;
		cout << "T5 " << E3 * K * exp(-27.0 / 164.0 * logt0) << endl;
	}

	return C1 * calc_mu1(5.0 / 82.0, h1, K)
		+ C2 * calc_mu2(17.0 / 328.0, h1, K, theta2, logt0) * exp(-3.0 / 656.0 * logt0)
		+ E1 * calc_mu1(87.0 / 164.0, h1, K) * exp(-27.0 / 328.0 * logt0)
		+ E2 * calc_mu2(5.0 / 246.0, h1, K, theta2, logt0) * exp(-13.0 / 246.0 * logt0)
		+ E3 * K * exp(-27.0 / 164.0 * logt0);
}

/* Calculates $C_5$ defined in equation (3.25) */
double calc_C5(double h1, double eta1, double theta1, double theta2, double logt0, bool verbose = false) {

	if (logt0 < log(100)) throw std::invalid_argument("logt0 must be >= log(100).");
	if (h1 <= 1) throw std::invalid_argument("h1 must be greater than 1.");
	if (eta1 <= 0) throw std::invalid_argument("eta1 must be positive.");
	if (theta1 <= 0) throw std::invalid_argument("theta1 must be positive.");
	if (theta2 <= 0) throw std::invalid_argument("theta2 must be positive.");

	double h0 = h1 / (1 - theta2 * exp(-7.0 / 17.0 * logt0));
	if (h0 <= 1 || h0 > 2) return numeric_limits<double>::infinity(); // Check that 1 < h_0 <= 2

	double C1 = calc_C1(h0, eta1, theta1, theta2, logt0);
	double C2 = calc_C2(h0, eta1, theta1, theta2, logt0);
	double E1 = calc_E1(h0, theta1, theta2, logt0);
	double E2 = calc_E2(h0, theta1, theta2, logt0);
	double E3 = calc_E3(h0, theta1, theta2, logt0);

	double K0 = (3.0 / 34.0 * logt0 - log(theta2 * sqrt(2 * Pi))) / log(h1) + 1;

	return C1 * calc_mu3(5.0 / 82.0, h1)
		+ C2 * calc_mu4(17.0 / 328.0, h1, theta2, logt0)
		+ E1 * calc_mu3(87.0 / 164.0, h1) * exp(-27.0 / 328.0 * logt0)
		+ E2 * calc_mu4(5.0 / 246.0, h1, theta2, logt0) * exp(-427.0 / 8364.0 * logt0)
		+ E3 * K0 * exp(-27.0 / 164.0 * logt0);
}

/* Calculates the final constant $A$ in the bound $|\zeta(1/2 + it)| \leq A t^{27/164}$ for $t_0 \leq t \leq t_1$ */
double calc_A(double h1, double h2, double eta1, double eta2, double theta1, double theta2, double theta3, double logt0, double logt1, bool verbose = false) {

	// Check parameters - if any bounds are violated, return the default maximum
	constexpr double _default = numeric_limits<double>::infinity();
	if (logt0 < log(100)) return _default;
	if (logt0 >= logt1) return _default;
	if (h1 <= 1)  return _default;
	if (h2 <= 1)  return _default;
	if (eta1 <= 0) return _default;
	if (eta2 <= 0) return _default;
	if (theta1 <= 0) throw _default;
	if (theta2 <= 0) throw _default;
	if (theta3 <= 0) throw _default;

	// Calculate the contribution from $S_1$ - Lemma 3.2
	double C0 = 2 * sqrt(theta3 + exp(-27.0 / 82.0 * logt0) / 2); 

	// Calculate the contribution from $S_2$ - Lemma 3.3
	double D3 = calc_D3(eta2, h2, theta2, theta3, logt0, logt1, verbose);
	double D4 = calc_D4(eta2, h2, theta2, theta3, logt0, logt1, verbose);
	double S2_coeff = D3 * exp((19.0 / 119.0 - 27.0 / 164.0) * logt0) + D4 * exp((71.0 / 476.0 - 27.0 / 164.0) * logt0);

	// Calculate the contribution from $S_3$ - Lemma 3.5
	double C4 = calc_C4(h1, eta1, theta1, theta2, logt0, logt1, verbose);

	// Calculate the contribution from the remainder term in the Riemann-Siegel formula - Lemma 3.1
	double R = 1.48 * exp((-1.0 / 4.0 - 27.0 / 164.0) * logt0) + 0.127 * exp((-3.0 / 4.0 - 27.0 / 164.0) * logt0)
		- sqrt(2) / exp(27.0 / 164.0 * logt0);

	if (verbose) {
		cout << "C0 " << C0 << endl;
		cout << "D3 " << D3 << endl;
		cout << "D4 " << D4 << endl;
		cout << "C4 " << C4 << endl;
		cout << "S2 " << S2_coeff << endl;
		cout << "Remainder " << R << endl;
	}

	return 2 * (C0 + S2_coeff + C4) + R;
}

/* Calculate the final constant $A$ in the bound $|\zeta(1/2 + it)| \leq A t^{27/164}$ for $t \geq t_0$ */
double calc_A_inf(double h1, double h2, double eta1, double eta2, double theta1, double theta2, double theta3, double logt0, bool verbose = false) {

	// Check parameters - if any bounds are violated, return the default maximum
	constexpr double _default = numeric_limits<double>::infinity();
	if (logt0 < log(100)) return _default;
	if (h1 <= 1)  return _default;
	if (h2 <= 1)  return _default;
	if (eta1 <= 0) return _default;
	if (eta2 <= 0) return _default;
	if (theta1 <= 0) throw _default;
	if (theta2 <= 0) throw _default;
	if (theta3 <= 0) throw _default;

	// Calculate the contribution from $S_1$ - Lemma 3.2
	double C0 = 2 * sqrt(theta3 * (1 + exp(-27.0 / 82.0 * logt0) / 2));

	// Calculate the contribution from $S_2$ - Lemma 3.3 with t_1 \to \infty
	double D3 = calc_D3_inf(eta2, h2, theta2, theta3, logt0, verbose);
	double D4 = calc_D4_inf(eta2, h2, theta2, theta3, logt0);
	double S2_coeff = D3 * exp((19.0 / 119.0 - 27.0 / 164.0) * logt0) + D4 * exp((71.0 / 476.0 - 27.0 / 164.0) * logt0);

	// Calculate the contribution from $S_3$ - Lemma 3.5
	double C5 = calc_C5(h1, eta1, theta1, theta2, logt0, verbose);

	// Calculate the contribution from the remainder term in the Riemann-Siegel formula - Lemma 3.1
	double R = 1.48 * exp((-1.0 / 4.0 - 27.0 / 164.0) * logt0) + 0.127 * exp((-3.0 / 4.0 - 27.0 / 164.0) * logt0)
		- sqrt(2) / exp(27.0 / 164.0 * logt0);

	if (verbose) {
		cout << "C0 " << C0 << endl;
		cout << "D3 " << D3 << endl;
		cout << "D4 " << D4 << endl;
		cout << "S2 " << S2_coeff << endl;
		cout << "C4 " << C5 << endl;
		cout << "Remainder " << R << endl;
	}

	return 2 * (C0 + S2_coeff + C5) + R;
}

/* 
Optimise parameters on a given interval $[t_0, t_1]$. 
This method uses a simple greedy stochastic gradient-free optimisation algorithm - 
in each iteration, the current optimal parameters are slightly perturbed (randomly), 
and if the perturbed parameters are valid and more favourable, then it is set as the 
new optimal parameter. 
*/
double optimise_interval(double logt0, double logt1, vector<double> param) {

	random_device rd;
	uniform_real_distribution<double> dis(-1, 1);

	// Candidate parameters
	vector<double> v(param.size(), 0);

	double best_const = calc_A(param[0], param[1], param[2], param[3], param[4], param[5], param[6], logt0, logt1, false);
	for (int i = 0; i < 1000000; ++i) {
		for (int d = 0; d < param.size(); ++d) {
			v[d] = param[d] * exp(0.05 * dis(rd));
		}
		double c = calc_A(v[0], v[1], v[2], v[3], v[4], v[5], v[6], logt0, logt1);
		if (best_const > c) {
			best_const = c;
			for (int d = 0; d < param.size(); ++d) { // copy
				param[d] = v[d];
			}
		}
	}
	
	// Round the best parameters to 6 significant figures
	for (int d = 0; d < param.size(); ++d) {
		v[d] = round(v[d], 6);
	}

	// Recalculate the constant A using the rounded parameters 
	double A = calc_A(v[0], v[1], v[2], v[3], v[4], v[5], v[6], logt0, logt1);

	// Print out found parameters and A constant in latex table format
	cout << "$" << logt0 << "$ & $" << logt1 << "$ ";
	for (int d = 0; d < param.size(); ++d) {
		cout << "& $" << param[d] << "$ ";
	}
	printf("& $%.2f$\\\\\n", round_upwards(A, 4));
	cout << "\\hline" << endl;

	return A;
}

/*
The same optimisation routine as above, except for the infinite interval $[t_0, \infty)$
*/
double optimise_infinite(double logt0) {
	random_device rd;
	uniform_real_distribution<double> dis(-1, 1);

	vector<double> param = { 1.01733, 1.00198, 1.68664, 0.895349, 1.34363, 5.41683e+06, 1.85412e-11 };
	vector<double> stepsize = { 0.001, 0.001, 0.01, 0.01, 0.01, 1000, 1e-6 };
	vector<double> v(param.size(), 0);

	double best_const = calc_A_inf(param[0], param[1], param[2], param[3], param[4], param[5], param[6], logt0);

	for (int i = 0; i < 1000000; ++i) {
		for (int d = 0; d < param.size(); ++d) {
			v[d] = param[d] * exp(0.05 * dis(rd));
		}
		double c = calc_A_inf(v[0], v[1], v[2], v[3], v[4], v[5], v[6], logt0);
		if (best_const > c) {
			best_const = c;
			for (int d = 0; d < param.size(); ++d) { // copy
				param[d] = v[d];
			}
			cout << best_const << endl;
		}
	}

	// print out 
	for (double vd : param) {
		cout << vd << ", ";
	}
	cout << endl;

	return best_const;
}


int main()
{
	// Optimisation code ----------------------------------------------------------------------------
	if (0) {
		vector<double> param = { 1.02932, 1.06726, 1.72183, 1.02275, 0.957426, 0.180062, 0.172999 };
		double logt0 = 60, logt1 = 65;
		while (logt1 < 1000) {
			double c = optimise_interval(logt0, logt1, param);
			logt0 = logt1;
			logt1 += 5;
		}
		optimise_infinite(875);
	}

	// Verification code ----------------------------------------------------------------------------
	if (0) {
		cout << setprecision(12);

		// h1, h2, eta1, eta2, theta1, theta2, theta3
		vector<double> param = { 1.02932, 1.06726, 1.72183, 1.02275, 0.957426, 0.180062, 0.172999 };
		double logt0 = 60, logt1 = 65;
		double _A = calc_A(param[0], param[1], param[2], param[3], param[4], param[5], param[6], logt0, logt1, true);
		cout << "A: " << _A << endl;
	}
}


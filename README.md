# Introduction

This repository contains code implementations referenced in the article "An explicit sub-Weyl bound for $\zeta(1/2 + it)$" by Dhir Patel and Andrew Yang (preprint available at https://arxiv.org/abs/2302.13444). The purpose of this code is to numerically verify that $$|\zeta(1/2 +it)| \leq 66.7 t^{27/164},\qquad(t \geq e^{60})$$
where $\zeta(s)$ denotes the Riemann zeta-function. This is used in the proof of Theorem 1.1 of the article. 

# Usage
The verification is divided into two parts. 

For $e^{60} \leq t \leq e^{875}$, the method

    double calc_A(double h1, double h2, double eta1, double eta2, double theta1, double theta2, double theta3, double logt0, double logt1, bool verbose = false)

is used to compute a constant $A$ such that $|\zeta(1/2 + it)| \leq A t^{27/164}$ for $t_0 \leq t \leq t_1$. The choices of $h_1$, $h_2$, $\eta_1$, $\eta_2$, $\theta_1$, $\theta_2$, $\theta_3$, $\log t_0$ and $\log t_1$ are given in Table 1 of the article.

For $t \geq e^{875}$, the method 

    double calc_A_inf(double h1, double h2, double eta1, double eta2, double theta1, double theta2, double theta3, double logt0, bool verbose = false)

is used to compute a constant $B$ such that $|\zeta(1/2 + it)| \leq Bt^{27/164}$ for all $t \geq t_0$. The parameter choices are provided in Section 3.1 of the article. Together, the two methods cover the range $t \ge e^{60}$. 

The parameters $h_1$, $h_2$, $\eta_1$, $\eta_2$, $\theta_1$, $\theta_2$, $\theta_3$ are found using a stochastic optimisation routine, implemented in 

    double optimise_interval(double logt0, double logt1, vector<double> param)
    double optimise_infinite(double logt0)

for the regions $[t_0, t_1]$ and $[t_0, \infty)$ respectively. The parameter `param` is an ordered list of starting values of $h_1$, $h_2$, $\eta_1$, $\eta_2$, $\theta_1$, $\theta_2$, $\theta_3$ where the search should begin.